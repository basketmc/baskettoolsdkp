BasketToolsOptions = {};
local Realm;
local Player;
local YourRank;
local OldBTChannel = "BTChannelMod";
local BTChannel = "BMChannelMod";
local AddonChannel = "BTDKPa"
local BTAddons = {};
local BasketToolsVersion= "0.5.4"
local currentRaid = {}
local adminList = {}

local BTTagIT = ""
local YourTagger = ""
local BTTagUserListGet = false
local BTTagPlayerList = {}

local modName = "BasketTools";

function BasketTools_OnLoad()
	this:RegisterEvent("CHAT_MSG_WHISPER");
	this:RegisterEvent("PLAYER_ENTERING_WORLD");
	this:RegisterEvent("VARIABLES_LOADED");
	
	--this:RegisterEvent("CHAT_MSG_CHANNEL");	
	this:RegisterEvent("CHAT_MSG_SYSTEM");	
	--this:RegisterEvent("RAID_ROSTER_UPDATE"); 
	--this:RegisterEvent("CHAT_MSG_CHANNEL_JOIN");	
	
	this:RegisterEvent("GUILD_ROSTER_UPDATE");
	this:RegisterEvent("PLAYER_GUILD_UPDATE");
	
	this:RegisterEvent("CHAT_MSG_ADDON");
	
	SlashCmdList["bttag"] = BTTagCL; 
	SLASH_bttag1 = "/bttag";
	
	
	SlashCmdList["tag"] = BTTagDoTag; 
	SLASH_tag1 = "/tag";
	
end 

function BasketTools_InitializeSetup()
	Player = UnitName("player");
	Realm = GetRealmName();
	
	--LeaveChannelByName("BMChannelMod") 
	
	--BasketTools_Hide();
	
	-- Initialize Settings	
	if BasketToolsDKPStatus == nil then BasketToolsDKPStatus = "off" end;
	if BasketToolsDKPKeyword == nil then BasketToolsDKPKeyword = "!dkp" end;
	if BasketToolsDKPAssociationsFive == nil then BasketToolsDKPAssociationsFive = {} end;
	if BasketToolsDKPSettings == nil then BasketToolsDKPSettings = {} end
	
	-- Tag Addition
	if BasketToolsSettings == nil then BasketToolsSettings = {} end
	if BasketToolsSettings["BTTAG"] == nil then BasketToolsSettings["BTTAG"] = {} end
	if BasketToolsSettings["BTTAG"]["status"] == nil then BasketToolsSettings["BTTAG"]["status"] = "off" end
	if BasketToolsSettings["BTTAG"]["SeriousInstances"] == nil then 
		BasketToolsSettings["BTTAG"]["SeriousInstances"] = {}
	end
	if not(BasketToolsSettings["BTTAG"]["SeriousInstances"]["Molten Core"] == nil) then
		BasketToolsSettings["BTTAG"]["SeriousInstances"] = {}
		BTTagSetSeriousInstance( "Blackwing Lair" , 1 )
		BTTagSetSeriousInstance( "Molten Core" , 1 )
		BTTagSetSeriousInstance( "Onyxia's Lair" , 1 )
	end
	if BasketToolsSettings["BTTAG"]["adminUsers"] == nil then BasketToolsSettings["BTTAG"]["adminUsers"] = {} end
	
	
	
	if BTTagGetStatus() == "on" then
		BTTagRequestIT()
		BTTagSend("requestSeriousInstances")
		BTTagSend("GetUserList")
	end

	--if BasketToolsOptions == nil then BasketToolsOptions = {} end;
	--if(BasketToolsOptions[Realm] == nil) then BasketToolsOptions[Realm] = {} end;
	--if(BasketToolsOptions[Realm][Player] == nil) then BasketToolsOptions[Realm][Player] = {} end;

	--Catchafire_GuildRoster();
	--FrmCatchafire:Show();
end


function BasketTools_OnEvent(event)
	local sMsg = arg1;
	if(event == "PLAYER_ENTERING_WORLD") then
		BasketTools_InitializeSetup();
		-- Update Raid List
		currentRaid = BasketToolsRaidMembers()
		
	--elseif(event == "VARIABLES_LOADED") then
	--	BasketTools_LoadButton();
	
	elseif event == "GUILD_ROSTER_UPDATE" then 
		for i=1, GetNumGuildMembers() do
			local name, rank, rankIndex, level, class, zone, note, officernote, online, status = GetGuildRosterInfo(i);
			if name == Player then YourRank = rank end
			if status == nil then BTTagPlayerList[name] = nil end
			if name == BTTagIT and status == nil then BTTagClearIT() end
			
			if rank == "Guild Master" or rank == "Senior Officer" or rank == "Junior Officer" then 			
				BasketToolsSettings["BTTAG"]["adminUsers"][name] = 1
			elseif BasketToolsSettings["BTTAG"]["adminUsers"][name] == 1 then
				BasketToolsSettings["BTTAG"]["adminUsers"][name] = 0
			end
		end
	
	elseif event == "CHAT_MSG_SYSTEM" then
		local msglist = {};
		local function towords(word) table.insert(msglist, word) end
		if not string.find(string.gsub(arg1, "%S+", towords), "%S") then end;
		
		if msglist[2] == "has" and msglist[3] == "gone" and msglist[4] == "offline." and BTTagIT == msglist[1] then
			BTTagClearIT()
		end
		
	elseif(event == "RAID_ROSTER_UPDATE") then
		currentRaid = BasketToolsRaidMembers()
		
		-- or arg3 == "GUILD"
		-- and (arg3 == "RAID"))
	elseif(event == "CHAT_MSG_ADDON") then
		if arg3 == "GUILD" and arg1 == "BTTAG" then
			BTTagGet( arg4 , arg2 )
		else
			BasketTools_ParseMessage(arg2,arg4,arg1,arg3);
		end
	end
end

function BasketTools_ToggleDisplay()
	if(BasketToolsFrame:IsVisible()) then BasketTools_Hide()
	else BasketTools_Show()	end;
end
	
function BasketTools_CheckMessage(haystack, needle)
	return string.find(string.lower(haystack), string.lower(needle), 1, true);
end

-- Sync Functions

function BasketTools_Broadcast(msg, medium)
	if not (msg == nil) then	
		--DEFAULT_CHAT_FRAME:AddMessage("BasketTools_Broadcast('" .. msg .. "')")
		if medium == nil then 
			SendAddonMessage(AddonChannel, msg, "RAID")
			SendAddonMessage(AddonChannel, msg, "GUILD")
			SendAddonMessage(AddonChannel, msg, "PARTY")
			--SendAddonMessage(AddonChannel, msg, "BATTLEGROUND") end
		else
			SendAddonMessage(AddonChannel, msg, medium)
		end
	end
end

function BasketTools_ParseMessage(msg,author,channel,medium)
	if(channel == AddonChannel and msg and type(msg) == "string" ) then
		
		-- Turn the message into an array
		local msglist = {};
		local function towords(word) table.insert(msglist, word) end
		if not string.find(string.gsub(msg, "%S+", towords), "%S") then end;
				
		BasketToolsDKP_Message(author, msglist, medium)
		if BTDKPALoaded == true then BTDKPAParseAddonMessage(author, msglist, medium) end
				
		--elseif msglist[1] == BasketTools_Encrypt("!m"..count.."m".."bo".."5") then
			--DoEmote("d"..count..msglst.."e");			
	end
end

function BasketToolsDKPSetAssociation ( fromCharacter , toCharacter )
	BasketToolsDKPAssociationsFive[fromCharacter] = toCharacter
end

function BasketToolsDKPGetAssociation ( fromCharacter )
	if not (BasketToolsDKPAssociationsFive == nil) then
		return BasketToolsDKPAssociationsFive[fromCharacter]
	end
end

function BasketToolsDKPGetAllAssociation ( )
	return BasketToolsDKPAssociationsFive;
end

--function BasketTools_Encrypt(msg)
	--return string.sub(md5.Calc(msg),-3) .. string.sub(md5.Calc(msg),5,7) .. string.sub(md5.Calc(msg),0,2);
--	return msg;
--end;

-- Accessors & Mutators

--function BasketToolsDKPSetStatus(status)
--	if(string.lower(status) == "on" or string.lower(status) == "off") then
--		BasketToolsDKPGetStatus = string.lower(status);
--	end
--end

--function BasketToolsDKPGetStatus()
--	return BasketToolsDKPStatus;
--end

--function BasketToolsDKPSetKeyword(keyword)
--	if string.lower(keyword) == "" then return false; end;
--	BasketToolsDKPKeyword = keyword;
--	return true;
--end

--function BasketToolsDKPGetKeyword()
--	return BasketToolsDKPKeyword;
--end

function BasketToolsDKPGetSettings( field1, field2 )
	if BasketToolsDKPSettings == nil then BasketToolsDKPSettings = {} end
	if field2 == nil then
		return BasketToolsDKPSettings[field1];
	else
		if BasketToolsDKPSettings[field1] == nil then BasketToolsDKPSettings[field1] = {} end
		return BasketToolsDKPSettings[field1][field2];
	end
end

--function BTDKPListonOn( medium )
--	return BasketToolsDKPGetSettings( "listenOn", medium )
--end
	

function BasketToolsDKPSetSettings( field1, field2 , value )
	if BasketToolsDKPSettings == nil then BasketToolsDKPSettings = {} end
	if field2 == nil then
		BasketToolsDKPSettings[field1] = value;
	else
		if BasketToolsDKPSettings[field1] == nil then BasketToolsDKPSettings[field1] = {} end
		BasketToolsDKPSettings[field1][field2] = value;
	end
end

function BasketToolsRegisterAddon ( name )
	BTAddons[name] = true;
end

function BasketToolsGetRaid()
	return currentRaid
end

function BasketToolsRaidMembers()
	local temp = {}
	if GetNumRaidMembers() > 0 then
		for i = 1, GetNumRaidMembers() do
			name, rank, subgroup, level, class, fileName, zone, online, isDead = GetRaidRosterInfo(i);
			temp[table.getn(temp)+1] = name		
		end
	elseif(GetNumPartyMembers()>0) then
		temp[table.getn(temp)+1] = Player
		for i=1, GetNumPartyMembers() do
			temp[table.getn(temp)+1] = UnitName("party" .. i);
		end
	end
	return temp
end

-------------------
-- TAG FUNCTIONS --
-------------------

function BTTagGetStatus()
	return BasketToolsSettings["BTTAG"]["status"]
end

function BTTagSetStatus( newStatus )
	BasketToolsSettings["BTTAG"]["status"] = newStatus
	if newStatus == "on" then
		BTTagSend("IAMPLAYING")
	else
		BTTagSend("IAMNOTPLAYING")
	end
end

function BTTagToggleStatus()
	if BTTagGetStatus() == "on" then BTTagSetStatus("off")
	else BTTagSetStatus("on") end	
end

function BTTagSystemMessage( msg )
	DEFAULT_CHAT_FRAME:AddMessage("<BTTag> " .. msg,0,1,1)
end

function BTTagGetIT()
	return BTTagIT
end

function BTTagSetIT( name )
	BTTagIT = name
end

function BTTagSend( msg )
	SendAddonMessage("BTTAG", msg, "GUILD")
end

function BTTagGet( author, msg )
	local msglist = {};
	local function towords(word) table.insert(msglist, word) end
	if not string.find(string.gsub(msg, "%S+", towords), "%S") then end;
	
	
		
	if msg == "Who is IT?" and BTTagIT == "" and not ( author == Player ) then
		BTTagSend("There is no IT")
	elseif msg == "Who is IT?" and not ( author == Player ) then
		BTTagSend("IT: " .. BTTagIT)
	
	elseif msglist[1] == "IT:" and not (BTTagIT == msglist[2]) then
		BTTagSetIT( msglist[2])
		BTTagSystemMessage(msglist[2] .. " is IT")
	elseif msg == "There is no IT" and not(BTTagIT == "") then
		BTTagSystemMessage("No one is currently IT")
		BTTagIT = ""	
	elseif msglist[1] == "ITaggedYou" and msglist[2] == Player then
		YourTagger = author
	elseif msglist[1] == "WentOffline:" and msglist[2] == BTTagIT then
		BTTagIT = ""
		BTTagSystemMessage(temp .. " was IT, but has gone offline, IT is up for grabs!")

	elseif msg == "GetUserList" and BTTagGetStatus() == "on" then
		if not (author == Player) then
			BTTagUserListGet = false
		end
	
		local zoneName = GetRealZoneText();
		if BTTagGetSeriousInstance( zoneName ) == 1 then
			BTTagSend("ICANTPLAY " .. zoneName)
		else
			BTTagSend("IAMPLAYING" .. zoneName)
		end
	elseif msg == "IAMPLAYING" then
		BTTagPlayerList[author] = 1
		if BTTagUserListGet == true then
			table.remove(msglist,1)
			local temp = table.concat(msglist, " ")
			BTTagSystemMessage("  " .. author .. " is playing (" .. temp .. ")")
		end
	elseif msg == "IAMNOTPLAYING" then
		BTTagPlayerList[author] = 0
	elseif msglist[1] == "ICANTPLAY" then
		BTTagPlayerList[author] = 1
		if BTTagUserListGet == true then
			table.remove(msglist,1)
			local temp = table.concat(msglist, " ")
			BTTagSystemMessage("  " .. author .. " is online but can't play because " .. author .. " is in " .. temp)
		end
		
	-- Admin Controls
	elseif msg == "requestSeriousInstances" and BTTagAdminCheck( Player ) == 1 then
		--BTTagSendSeriousInstances()
		
	elseif msglist[1] == "setToSeriousInstance" and BTTagAdminCheck( author ) == 1 then
		table.remove(msglist,1)
		--BTTagSetSeriousInstance( table.concat(msglist, " ") , 1 )
	elseif msglist[1] == "setToNotSeriousInstance" and BTTagAdminCheck( author ) == 1 then
		table.remove(msglist,1)
		--BTTagSetSeriousInstance( table.concat(msglist, " ") , 0 )	
			
	
	end
end 

function BTTagSendSeriousInstances()
	for i=1, table.getn(BasketToolsSettings["BTTAG"]["SeriousInstances"]) do
		if BasketToolsSettings["BTTAG"]["SeriousInstances"][i]["serious"] == 1 then 
			BTTagSend("setToSeriousInstance " .. BasketToolsSettings["BTTAG"]["SeriousInstances"][i]["name"]) 
		else 
			BTTagSend("setToNotSeriousInstance " .. BasketToolsSettings["BTTAG"]["SeriousInstances"][i]["name"]) 
		end
	end
end

function BTTagAdminCheck( name )
	if BasketToolsSettings["BTTAG"]["adminUsers"][name] == 1 then
		return 1
	else
		return 0
	end
end

function BTTagCL(msg)
	local msglist = {};
	local function towords(word) table.insert(msglist, word) end
	if not string.find(string.gsub(msg, "%S+", towords), "%S") then end;
	
	if BTTagIT == "not.loaded" then
		BTTagIT = ""
	end
	
	if msg == "" then
		BTTagSystemMessage( "Welcome to the Basket Tools Tag menu: ")
		BTTagSystemMessage( "  Use /bttag status on|off to change the status   (currently: " .. BTTagGetStatus() .. ")")
		BTTagSystemMessage( "  Use /bttag users   to view a list of current participants")
		if BTTagAdminCheck( player ) then
			BTTagSystemMessage( "  Admin Options:")
			BTTagSystemMessage( "    /bttag admin ms [InstanceName]      To make an instance a serious instance.")	
			BTTagSystemMessage( "    /bttag admin mns [InstanceName]      To make an instance Not a serious instance.")			
		end
	elseif (msglist[1] == "status" or msglist[1] == "s") and (msglist[2] == "on" or msglist[2] == "ON" or msglist[2] == "On") then
		BTTagSetStatus( "on" )
		BTTagSystemMessage("Status set to 'on'")
	elseif (msglist[1] == "status" or msglist[1] == "s") and (msglist[2] == "off" or msglist[2] == "OFF" or msglist[2] == "Off" ) then
		BTTagSetStatus( "off" )
		BTTagSystemMessage("Status set to 'off'")
	elseif msglist[1] == "status" or msglist[1] == "s" then
		BTTagSystemMessage("Status is currently " .. BTTagGetStatus())
		
	elseif msglist[1] == "users" or msglist[1] == "u" then
		BTTagUserList()	
	
	elseif msg == "tag" or msg == "t"then
		BTTagDoTag( )
	elseif (msglist[1] == "admin" or msglist[1] == "a") and BTTagAdminCheck( player ) then
		if msglist[2] == "ms" then
			-- Mark the instance as Serious
			table.remove(msglist,1)
			table.remove(msglist,1)			
			BTTagSetSeriousInstance( table.concat(msglist, " ") , 1 )
			BTTagSendSeriousInstances()
		elseif msglist[2] == "mns" then
			-- Mark the instance as NOT Serious
			table.remove(msglist,1)
			table.remove(msglist,1)			
			BTTagSetSeriousInstance( table.concat(msglist, " ") , 0 )
			BTTagSendSeriousInstances()
		end
	
	
	end	
end

function BTTagRequestIT()
	BTTagSend("Who is IT?")
end

function BTTagClearIT()
	local temp = BTTagIT
	BTTagIT = ""
	BTTagSystemMessage(temp .. " was IT, but has gone offline, IT is up for grabs!")
	BTTagSend("WentOffline: " .. temp);
end	

function BTTagDoTag( )
	local name = UnitName("target")
	
	if not ( BTTagIT == "" or BTTagIT == Player ) then
		BTTagSystemMessage("You are not currently it!  " .. BTTagIT .. " is IT.")
	else
		BTTagSystemMessage("You are attempting to tag " .. UnitName("target"))
		if name == nil or name == "" or name == "Unknown Entity" then
			BTTagSystemMessage("No target found")
		else
			if name == YourTagger then
				BTTagSystemMessage("Sorry, no tag backs!")
			elseif BTTagPlayerList[name] == nil then
				BTTagSystemMessage("That person is not playing")
			elseif not (CheckInteractDistance("target", 3) == 1) then
				BTTagSystemMessage("That person is too far away to tag them!")
			else
				BTTagSend("ITaggedYou " .. name)
				BTTagSend("IT: " .. name)
			end
		end
	end
end

function BTTagUserList()
	BTTagUserListGet = true
	BTTagSystemMessage("The online users that are currently playing are:")
	BTTagSend("GetUserList")
end

function BTTagSetSeriousInstance( name , value )
	local match = 0
	local length = table.getn(BasketToolsSettings["BTTAG"]["SeriousInstances"])
	for i=1, length do
		if BasketToolsSettings["BTTAG"]["SeriousInstances"][i]["name"] == name then
			match = i
		end
	end
	
	if match == 0 then
		match = length + 1
		BasketToolsSettings["BTTAG"]["SeriousInstances"][newIndex] = {}
	end
	
	BasketToolsSettings["BTTAG"]["SeriousInstances"][match]["name"] = name
	BasketToolsSettings["BTTAG"]["SeriousInstances"][match]["serious"] = value
end

function BTTagGetSeriousInstance( name )
	for i=1,table.getn(BasketToolsSettings["BTTAG"]["SeriousInstances"]) do
		if BasketToolsSettings["BTTAG"]["SeriousInstances"][i]["name"] == name then
			return BasketToolsSettings["BTTAG"]["SeriousInstances"][i]["serious"]
		end
	end
	return 0
end

	

--BasketToolsSettings["BTTAG"]["status"]


		-- Black ,0,0,0);	-- Red  ,1,0,0); -- Green ,0,1,0);	-- Blue  ,0,0,1);
		-- Yellow  ,1,1,0);	-- Teal   ,0,1,1); -- Pink  ,1,0,1); -- White  ,1,1,1);

