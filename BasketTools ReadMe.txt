--[[

This tool is no longer maintained and likely doesn't work.
It is here so you can look at the code.  No support provided.

----------------------------------------------------
                     Features

BasketTool DKP View is an addon for WoW that displays to the 
user their DKP points.  In it's smallest form it provides a 
"Roll DKP" button which will do a /roll out of your DKP points
(making out at a set amount, default is 100).  In it's expanded
form you're able to run searches to see what other people's
points are.  You can easier search for characters, partical
character names, or class types that are in your current 
raid/group.  A user is able to input what the username is for
them on the DKP site, for use with alts.

The information for the addon is currently refresh by me, I've 
written a php script that parses the MTD DKP website and 
turns that information into something useable by the addon.

The addon is setup to sync up with other people who are using the
addon to populate the current DKP information as well as 
aliases used by other people (for DKP searches).

----------------------------------------------------
                   Installation

Unzip the two folders are place them into your 
Interface\AddOns folder, so you'll have these two folders:
Interface\AddOns\BasketTools
Interface\AddOns\BasketToolsDKP

Be sure that when you are at the character selection screen
that these two addons are enabled (by accessing the "Addon" button)

If you do not see the DKP interface right away, you can either
type in /dkp to open it, or you if you are using CTMod there should
be a DKP View button on the Misc. (last) tab.

When you are in the interface you can update the information
by hitting the "Look for updates" button.  If there is someone 
online with newer information then your addon will update.

----------------------------------------------------
               Version Descriptions

0.5.4
 - Fixed various outstanding bugs
 - Fixed known bugs with new WoW patch
0.5.3
 - Fixed the max rolls to 200 for BWL/AQ40
 - Auto Detect for MC/BWL/Ony button swapping
 - Fixed some other stuff
0.5.2
 - Changed from using Channel to using Addon messaging system
 - Added MC/BWL Account selector
 - Upgraded Admin Interface
   - Uses MC/BWL Account selector from client
   - Roll Monitor: Interface which displays rolls and sorts them
   - Roll Validator: Different validation rules depending on MC/BWL
     Plays message in Raid chat if rolling error detected
0.5.1
 - Corrected DKP Roll function to use Blue Mountain DKP system rules
0.5.0
 - Changed network channel.  Now designed for Blue Mountain DKP System
0.4.9
 - Fixed a bug that wouldn't let you search classes with fresh installs 
   (thanks hashx for finding my bug)
 - Hopefully fixed the "new version" message from displaying when you have the addon hiding
0.4.8
 - Some more Interfaces with the administration addon (if someone is manning the admin addon)
 - 1.10 support
0.4.7
 - Made the dkp Round decimal instead of floor ( .5+ rounds up)
 - Added button to close search results box
 - Interfaces with the administration addon (if someone is manning the admin addon)
   - instant dkp updates
   - set raids dkp max
   - setup unknown aliasing (for those not running addon) 

0.4.6
 - All maximize randomly should be fixed
 - Attempted to make basket tools join channel higher than 1 and 2
 - Changed update process to be more efficient

0.4.5
 - Changed the way the update works so that it wouldn't kick the updater offline
 - Added a "new version available" bar when a new version is detected for download
 - Fixed Maximize on Update
 - Fixed checkbox to hold value for "confirm on update"
 - Shortened "Roll DKP" to "Roll"
 - Made interface not cover the bags
 - Changed "Your DKP : xx" to "DKP: xx"
 - Added /dkp [search] to allow it to search via command line

0.4.4
 - Successfully removed CT_MasterMod dependancy 
  
0.4.3
 - Added aliasing
   - On alias others are informed of your alias
   - When doing a class search unknown members will prompt an alias request
   - Alias matches show in interface searches
   - Updates to alias trigger the search to refresh
   - Added a request all alias command which allows for quick population
 - Fixed crashes on alias to unknown character name
 - Fixed sorting with aliased members
			   
0.4.2
 - removed /basket command
 - fixed errors
 - Partical Search (inev for inevitable)
 - Fixed scroll box
 - Removed CT_MasterMod dependancy (then put it back in to fix scroll)
 
0.4.1
 - fixed update
 - removed errors
 - added "enter" removes focus on dialog boxes


----------------------------------------------------
Items to add:

 - Option to change transparency value
 - Checkbox to fix location
 - Checkbox for if auto update

----------------------------------------------------

]]--