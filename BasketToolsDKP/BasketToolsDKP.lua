local Realm;
local Player;
local Debug = false;
local DKPSearch = "";
local SyncLoading = false;
local SyncFrom = "";
local SyncMedium = "";
local SyncSending = false;
local SyncTo = "";
local SyncSendCache = {};
local SyncCache = {};
local version = "0.5.4";
local modName = "DKP View";
local newVersionAvailable = nil;
local aliasRequestTracker = {}
local BMRollBoost = 50


-- Gui Variables
local GuiHeightWithResults = 148;
local GuiHeightWithoutResults = 50;
local GuiHeightCurrent = GuiHeightWithoutResults;
local GuiShowResultsBox = false;
local GuiShowOptionsBox = false;

function BasketToolsDKP_OnLoad()
	--this:RegisterEvent("CHAT_MSG_WHISPER");
	this:RegisterEvent("CHAT_MSG_PARTY");
	this:RegisterEvent("CHAT_MSG_RAID");
	this:RegisterEvent("CHAT_MSG_CHANNEL_JOIN"); 
	this:RegisterEvent("VARIABLES_LOADED"); 
	this:RegisterEvent("PLAYER_ENTERING_WORLD");
	this:RegisterEvent("ZONE_CHANGED_NEW_AREA");
	
	SlashCmdList["BasketToolsDKP"] = BasketToolsDKP_CommandLine; 
	SLASH_BasketToolsDKP1 = "/dkp";
	
	--SlashCmdList["BasketToolsDKPUpdate"] = BasketToolsDKP_RequestSync; 
	--SLASH_BasketToolsDKPUpdate1 = "/dkpupdate";
	
	DEFAULT_CHAT_FRAME:AddMessage("Enabled BasketTools " .. modName .. ".  To display use /dkp",0,1,0);
	
	Player = UnitName("player");
	Realm = GetRealmName();
end 

function BasketToolsDKP_OnEvent(event)

	if(event == "PLAYER_ENTERING_WORLD") then
		BasketToolsDKP_RequestSync(); -- auto update
	end
		
		
	if(event == "VARIABLES_LOADED") then
		BasketToolsRegisterAddon ( "BTDKP" );
		if BasketToolsDKPClasses == nil then BasketToolsDKPClasses = {} end
		if BasketToolsDKPCharactersFive == nil then BasketToolsDKPCharactersFive = {} end
		if BasketToolsDKPUpdated == nil then BasketToolsDKPUpdated = 0 end
		
		if(CT_RegisterMod) then CT_RegisterMod(modName, "DKP Viewer", 5, "Interface\\Icons\\Spell_Ice_Lament", "In-game DKP Viewer + Sync", "switch", "", BasketToolsDKP_ToggleDisplay); end
		
		-- Default values if unset
		if BasketToolsDKPGetSettings("gui", nil) == nil then BasketToolsDKPSetSettings("gui", nil, {} ) end
		if BasketToolsDKPGetSettings("gui", "hidden") == nil then BasketToolsDKPSetSettings("gui", "hidden", 0) end
		if BasketToolsDKPGetSettings("gui", "minimize") == nil then BasketToolsDKPSetSettings("gui", "minimize", 0) end
		
		if BasketToolsDKPGetSettings("gui", "dkpAccount") == nil then BasketToolsDKPSetAccount( "xmcx" ) end
		
		if BasketToolsDKPGetSettings( "maxdkproll" , nil ) == nil then BasketToolsDKPSetSettings( "maxdkproll" , nil , "100" ) end
		if BasketToolsDKPGetSettings( "confirmSync" , nil ) == nil then BasketToolsDKPSetSettings( "confirmSync" , nil, false ) end
		if BasketToolsDKPGetSettings( "alias" , nil ) == nil then BasketToolsDKPSetSettings( "alias" , nil, UnitName("player") ) end
		
		-- GUI Stuff
		BasketToolsDKPFrame:SetAlpha(1);
		BasketToolsDKPFrame:SetBackdropBorderColor(0, 0, 0, 0.6);
		BasketToolsDKPFrame:SetBackdropColor(0, 0, 0, 0.6);		
		BasketToolsDKPFrameSyncConfirmBox:SetAlpha(1);
		BasketToolsDKPFrameSyncConfirmBox:SetBackdropBorderColor(0, 0, 0, 0.6);
		BasketToolsDKPFrameSyncConfirmBox:SetBackdropColor(0, 0, 0, 0.6);	
		BasketToolsDKPFrameNewVersionAvailable:SetAlpha(1);
		BasketToolsDKPFrameNewVersionAvailable:SetBackdropBorderColor(0, 0, 0, 0.6);
		BasketToolsDKPFrameNewVersionAvailable:SetBackdropColor(0, 0, 0, 0.6);			
		BasketToolsDKPMaxDKPRoll:SetText(BasketToolsDKPGetSettings( "maxdkproll" , nil ));
		BasketToolsDKPAlias:SetText(BasketToolsDKPGetSettings( "alias" , nil ));
		BasketToolsDKPConfirmSync:SetChecked(BasketToolsDKPGetSettings( "confirmSync" , nil ));		
		BasketToolsDKPFrameResults:SetText("");		
		BasketToolsDKPSearchText:SetText("Search : ");
		BasketToolsDKPSearchText:SetPoint("TOPRIGHT",BasketToolsDKPSearch,"TOPLEFT",-4,0); 
		BasketToolsDKPMainText:SetText("DKP View");
		BasketToolsDKPVersionText:SetText(version);
		
		BasketToolsDKPYourDKP();
		
		if BasketToolsDKPSettings["gui"]["hidden"] == 1 then BasketToolsDKP_Hide()
		else BasketToolsDKP_Show() end
		if BasketToolsDKPSettings["gui"]["minimize"] == 1 then BasketToolsDKP_Minimize( true )
		else BasketToolsDKP_Minimize( false ) end
		
		BasketToolsDKPSetAccount( BasketToolsDKPGetAccount() )
		
		BasketToolsDKP_RequestSync();
	elseif(event == "PLAYER_ENTERING_WORLD" or event == "CHAT_MSG_CHANNEL_JOIN" or event == "CHAT_MSG_PARTY" or event == "CHAT_MSG_RAID") then
		--BasketToolsDKP_RequestSync(); -- auto update
		
		-- This will be used to auto select the dkp account selector
		--DEFAULT_CHAT_FRAME:AddMessage("You just entered : " .. GetCurrentMapZone());
		
		-- Get instance name
		
		-- commented out because on a sync on load it seemed to kick you offline
		-- BasketToolsDKP_Broadcast("PING");
		
		local zoneName = GetRealZoneText();
		--DEFAULT_CHAT_FRAME:AddMessage("debug " .. zoneName);
		if zoneName == "Blackwing Lair" and not (BasketToolsDKPGetAccount() == "xbwlx")  then
			BasketToolsDKPSetAccount( "xbwlx" )
			DEFAULT_CHAT_FRAME:AddMessage("<< BasketTools >> Zone change detected.  Changing dkp account and rolls to Blackwing Lair")
		elseif zoneName == "Temple of Ahn'Qiraj" and not (BasketToolsDKPGetAccount() == "xbwlx")  then
			BasketToolsDKPSetAccount( "xbwlx" )
			DEFAULT_CHAT_FRAME:AddMessage("<< BasketTools >> Zone change detected.  Changing dkp account and rolls to Temple of Ahn'Qiraj")
		elseif zoneName == "Molten Core" and not (BasketToolsDKPGetAccount() == "xmcx") then
			BasketToolsDKPSetAccount( "xmcx" )		
			DEFAULT_CHAT_FRAME:AddMessage("<< BasketTools >> Zone change detected.  Changing dkp account and rolls to Molten Core")	
		elseif zoneName == "Onyxia's Lair" and not (BasketToolsDKPGetAccount() == "xmcx") then
			BasketToolsDKPSetAccount( "xmcx" )		
			DEFAULT_CHAT_FRAME:AddMessage("<< BasketTools >> Zone change detected.  Changing dkp account and rolls to Onyxia's Lair")	
		end
			
		
--	elseif(BasketToolsDKPGetStatus() == "on") then
--		local what = arg1;
--		local who = arg2;
--		local method = "";		
--		if(event == "CHAT_MSG_WHISPER") then method = "WHISPER"; 	
--		elseif(event == "CHAT_MSG_PARTY") then method = "PARTY";
--		elseif(event == "CHAT_MSG_RAID") then method = "RAID";
--		elseif(event == "CHAT_MSG_GUILD") then method = "GUILD"; end;
		
--		if(BasketTools_CheckMessage(what, BasketToolsDKPGetKeyword())) then 
--			BasketToolsDKP_SendResults(BasketToolsDKP_Respond(what, who) , method );
--		end
	end
	-- minimize / maximize
	if BasketToolsDKPGetSettings( "gui" , "minimize" ) == 1 then BasketToolsDKP_Minimize( true )
	else BasketToolsDKP_Minimize( false ) end
	
	-- hide / unhide
 	if BasketToolsDKPGetSettings("gui","hidden") == 1 then BasketToolsDKP_Hide()
	else BasketToolsDKP_Show() end
end

function BasketToolsDKP_CommandLine(msg)
	BasketToolsDKPSearch:SetText("DKPSearch")
	BasketToolsDKP_Show()
	BasketToolsDKP_Minimize( false )
	if not (msg == "") then
		DKPSearch = msg
		BasketToolsDKP_DisplayResults( )
	end
end


-- Obseleting
--function BasketToolsDKP_Broadcast(msg)
--	BasketTools_Broadcast(msg);
--	DEFAULT_CHAT_FRAME:AddMessage("This line should not run, if it does, let Basket know what made it occur"); end; 
--end

function BasketToolsDKP_RequestSync()
	-- Request sync on all channels
	BasketTools_Broadcast("Sync Version " .. BasketToolsDKPUpdated .. " " .. version);
	BasketToolsDKPConfirmSyncUpdateText:SetText("Looking for update...");
	SyncLoading = false;
	SyncSending = false;
end

function BasketToolsDKP_SendSync()
	local count = 1;
	local message = "";
	SyncSendCache = {};
	-- Begin
	BasketToolsDKPConfirmSyncUpdateText:SetText("Sending Update...");
	SyncSendCache[table.getn(SyncSendCache)+1] = "Sync Begin " .. BasketToolsDKPUpdated

	-- Send Characters
	for key, val in pairs(BasketToolsDKPCharactersFive) do
		message = message .. " " .. key .. " " .. val;
		if count == 8 then	
			SyncSendCache[table.getn(SyncSendCache)+1] = "Sync CharBody" .. message;
			message = "";
			count = 1;
		end
		count=count+1;
	end
	if not (message == "") then
		SyncSendCache[table.getn(SyncSendCache)+1] = "Sync CharBody" .. message;
	end
	
	-- Ending
	SyncSendCache[table.getn(SyncSendCache)+1] = "Sync End " .. BasketToolsDKPUpdated;
end

function BasketToolsDKP_Message(author, msglist, medium)
	local temp = table.concat(msglist, " ");
	--DEFAULT_CHAT_FRAME:AddMessage("BTDKP just received message \"" .. temp .. "\"");
	
	-- later check to make sure it's not you, don't bother checking your own message
	-- later.  maybe write a random number of message to wait before sending a "begin"
	
	if msglist[1] == "Sync" then
	
		-- client/server version checking
		if msglist[2] == "Version" then
			BasketToolsDKPCheckVersion(msglist[4]) -- validate if they a better "version" (gui)
			if not SyncLoading and not SyncSending then
				if tonumber(msglist[3]) > tonumber(BasketToolsDKPUpdated) then
					BasketToolsDKP_RequestSync()
				elseif tonumber(msglist[3]) < tonumber(BasketToolsDKPUpdated) then
				 	-- generate the table of messages
					BasketToolsDKP_SendSync()
					if table.getn(SyncSendCache) then
						-- send the server begin message to all channels
						BasketTools_Broadcast(SyncSendCache[1], medium)
						SyncSending = true
						SyncTo = nil
						SyncMedium = medium
						table.remove(SyncSendCache,1)
					end
				end
			end
		
		-- server receives 200
		elseif SyncSending and msglist[2] == "200" and table.getn(SyncSendCache) > 0 and ( SyncTo == nil or SyncTo == author ) then
			if msglist[3] == Player and ( medium == SyncMedium or SyncMedium == "") then
				SyncTo = author
				SyncMedium = medium
				for i=1, 5 do
					if table.getn(SyncSendCache) > 0 then
						BasketTools_Broadcast(SyncSendCache[1])
						table.remove(SyncSendCache,1)			
					end
				end
				if table.getn(SyncSendCache) == 0 then			
					BasketToolsDKPConfirmSyncUpdateText:SetText("Update Sent.")
				else
					BasketTools_Broadcast("Sync OK? " .. SyncTo, SyncMedium);
				end
			-- client is getting update from someone else
			elseif SyncTo == author then
				SyncSending = false
				SyncTo = nil
				SyncMedium = ""
				SyncSendCache = {}				
			end
		
		-- server receives Thanks!
		elseif SyncSending and msglist[2] == "Thanks!" and SyncTo == author and SyncMedium == medium then
			SyncSending = false
			SyncTo = nil
			SyncMedium = ""
			SyncSendCache = {}	
			BasketToolsDKPConfirmSyncUpdateText:SetText("Update Sent.")			
			
		-- client receives update beginning tag
		elseif msglist[2] == "Begin" and tonumber(msglist[3]) > tonumber(BasketToolsDKPUpdated)
			and ( tonumber(SyncCache["version"]) == nil or tonumber(SyncCache["version"]) == 0 or tonumber(SyncCache["version"]) < tonumber(msglist[3]))  then
			SyncLoading = true
			SyncFrom = author
			
			--DEFAULT_CHAT_FRAME:AddMessage("blah blah " .. medium)
			
			SyncMedium = medium
			SyncCache = {}
			SyncCache["chars"] = {}; 
			SyncCache["classes"] = {}; 
			SyncCache["version"] = msglist[3];
			BasketToolsDKPConfirmSyncUpdateText:SetText("Receiving Update...")
			BasketTools_Broadcast( "Sync 200 " .. author, SyncMedium)
			
		-- client receives character dkp
		elseif SyncLoading and SyncFrom == author and msglist[2] == "CharBody" then
			local i = 3;
			while not (msglist[i] == nil) do
				--BasketToolsDKPCharactersFive[msglist[i]] = msglist[(i+1)];
				SyncCache["chars"][msglist[i]] = tonumber(msglist[(i+1)]);
				i=i+2
			end
			
		-- client is asked if they're ok
		elseif SyncLoading and SyncFrom == author and msglist[2] == "OK?" and msglist[3] == Player then
			BasketTools_Broadcast( "Sync 200 " .. author, SyncMedium);
		
		-- Client receives update ending tag
		elseif SyncLoading and SyncFrom == author and msglist[2] == "End" then
	
			BasketTools_Broadcast("Sync Thanks!", SyncMedium)
			
			SyncLoading = false;
			SyncMedium = "";
			SyncCache["version"] = tonumber(msglist[3])
			
			local confirmSync = BasketToolsDKPGetSettings( "confirmSync" , nil )
			if not confirmSync then
				BasketToolsDKP_ApplyCache();
			else
				BasketToolsDKPFrameSyncConfirmBox:Show();
				if BasketToolsDKPGetSettings( "gui" , "minimize" ) == 1 then
					BasketToolsDKP_Minimize( true );
				else 
					BasketToolsDKP_Minimize( false );
				end
				BasketToolsDKPConfirmSyncUpdateText:SetText("Awaiting Confirmation.");
				BasketToolsDKPFrameSyncConfirmBoxText:SetText("Would you like to apply this update? (Provided by " .. author .. ")");
			end

		-- alias stuff
		elseif msglist[2] == "RequestAlias" and not (Player == author) and not ( msglist[3] == nil ) and not ( BasketToolsDKPGetAssociation(msglist[3]) == nil ) then
			BasketTools_Broadcast( "Sync SetAlias " .. msglist[3] .. " " .. BasketToolsDKPGetAssociation(msglist[3]), medium)
		elseif msglist[2] == "SetAlias" and not ( msglist[3] == nil ) and not ( msglist[4] == nil) then
			BasketToolsDKPSetAssociation( msglist[3] , msglist[4] )
			if BTDKPALoaded == true then BTDKPAPaintLootTracker() end
			if not (DKPSearch == "" ) then BasketToolsDKP_DisplayResults() end;
		end	
	elseif(msglist[1] == "PING") then
		BasketTools_Broadcast("PONG " .. version)		
	end
end	

function BasketToolsDKPCheckVersion(input)
	local compare1 = {}
	local count = 1;
	if not (input == nil) then
		for word in string.gmatch(input, ".") do
			if not (word == ".") then 
				if compare1[count] == nil then compare1[count] = word
				else compare1[count] = compare1[count] .. "word" end
			else count=count+1 end
		end
		local compare2 = {}
		count = 1;
		for word in string.gmatch(version, ".") do
			if not (word == ".") then 
				if compare2[count] == nil then compare2[count] = word
				else compare2[count] = compare2[count] .. "word" end
			else count=count+1 end
		end
		
		for i=1 , table.getn(compare1) do
			if compare1[i] > compare2[i] then
				newVersionAvailable = input
				
				if BasketToolsDKPSettings["gui"]["hidden"] == 1 then BasketToolsDKP_Hide()
				else BasketToolsDKP_Show() end
				if BasketToolsDKPSettings["gui"]["minimize"] == 1 then BasketToolsDKP_Minimize( true )
				else BasketToolsDKP_Minimize( false ) end
		
				BasketToolsDKPFrameNewVersionAvailableText:SetText("Version " .. newVersionAvailable .. " is now\navailable for download!  :)"); -- at bluemountainguild.com");
				--BasketToolsDKPFrameNewVersionAvailable:Show()			
				--DEFAULT_CHAT_FRAME:AddMessage(compare1[i] .. " compared to " .. compare2[i])		
			end
		end
	end
end

function BasketToolsDKPSetAlias ( inputAlias )
	BasketToolsDKPSetSettings( "alias" , nil, inputAlias );
	BasketToolsDKPYourDKP();
	--DEFAULT_CHAT_FRAME:AddMessage("soooo....")
	if not (BasketToolsDKPCharactersFive[inputAlias .. BasketToolsDKPGetAccount()] == nil) and not (BasketToolsDKPGetAssociation(Player) == inputAlias)  then
		BasketToolsDKPSetAssociation(Player , inputAlias )
		BasketTools_Broadcast( "Sync SetAlias " .. Player .. " " .. inputAlias)
	end
end

function BasketToolsDKPGetAccount()
	return BasketToolsDKPGetSettings("gui", "dkpAccount")
end

function BasketToolsDKPSetAccount( account )
	BasketToolsDKPSetSettings("gui", "dkpAccount", account)
	if( account == "xmcx") then
		getglobal("BTDKPAccountRadioMC"):SetChecked(true);
		getglobal("BTDKPAccountRadioBWL"):SetChecked(false);
	elseif ( account == "xbwlx") then
		getglobal("BTDKPAccountRadioMC"):SetChecked(false);
		getglobal("BTDKPAccountRadioBWL"):SetChecked(true);
	end
	BasketToolsDKP_DisplayResults( )
	BasketToolsDKPYourDKP()
end

function BasketToolsDKPGetDKP ( name )
	if not ( name == "" ) then
		--DEFAULT_CHAT_FRAME:AddMessage("Debug : " .. name)
		--DEFAULT_CHAT_FRAME:AddMessage("Debug : " .. BasketToolsDKPGetAssociation(name)) 
		
		if not (BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] == nil) then
			return BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()]
		elseif not (BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] == nil) then
			return BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()]
		elseif not ( BasketToolsDKPGetAssociation(name) == nil ) and not (BasketToolsDKPCharactersFive[BasketToolsDKPGetAssociation(name) .. BasketToolsDKPGetAccount()] == nil) then
			return BasketToolsDKPCharactersFive[BasketToolsDKPGetAssociation(name) .. BasketToolsDKPGetAccount()]
		end
		if not (name == nil) then BasketToolsDKPRequestAlias( name ) end
	end
	return nil
end

function BasketToolsDKPRequestAlias( name )
	if not ( name == nil ) and aliasRequestTracker[name] == nil then
		BasketTools_Broadcast("Sync RequestAlias " .. name);
		aliasRequestTracker[name] = 1
	end
end

function BasketToolsDKP_AddCharDKP ( name , dkp )
	if BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] == nil then BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] = 0 end
	BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] = BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] + dkp
end

function BasketToolsDKP_GetVersion ( )
	return BasketToolsDKPUpdated
end

function BasketToolsDKP_SetVersion( dkp )
	BasketToolsDKPUpdated = dkp
end

function BasketToolsDKP_ApplyCache()
	BasketToolsDKPConfirmSyncUpdateText:SetText("Applying Update...");
	if not (SyncCache["version"] == nil) and not (tonumber(SyncCache["version"]) == 0) then
		if BasketToolsDKPCharactersFive == nil then BasketToolsDKPCharactersFive = {}; end
		
		-- Empty DKP Data
		BasketToolsDKPCharactersFive = {};
		
		for key, val in pairs(SyncCache["chars"]) do
			BasketToolsDKPCharactersFive[key] = val;
		end
		for key, val in pairs(SyncCache["classes"]) do
			BasketToolsDKPClasses[key] = {};
			for i=1 , table.getn(val) do
				BasketToolsDKPClasses[key][i] = val[i];
				--DEFAULT_CHAT_FRAME:AddMessage("Debug : key = " .. key .. " ; val[i] = " .. val[i]);
			end
		end	
		BasketToolsDKPUpdated = tonumber(SyncCache["version"]);
		-- Clear Cache
		BasketToolsDKP_ClearCache()
	end
	BasketToolsDKPYourDKP();
	BasketToolsDKPConfirmSyncUpdateText:SetText("Update Complete.");
end

function BasketToolsDKP_ClearCache()
	SyncCache = {}
end

function BasketToolsDKP_Respond(what, who)
	local characters = {};
	local dkp = {};
	local history = {};
	local temp = "";
	
	local msglist = {};
	local function towords(word) table.insert(msglist, word) end
	if not string.find(string.gsub(what, "%S+", towords), "%S") then end;
	
	local groupMembers = {};
	if(GetNumRaidMembers()>0) then
		for i=1, GetNumRaidMembers() do
			name, rank, subgroup, level, class, fileName, zone, online, isDead = GetRaidRosterInfo(i);
			groupMembers[table.getn(groupMembers)+1] = { name , class };
		end
	elseif(GetNumPartyMembers()>0) then
		for i=1, GetNumPartyMembers() do
			groupMembers[table.getn(groupMembers)+1] = { UnitName("party" .. i) , UnitClass("party" .. i) };
		end
	end
	
	-- Check for class name in string with members of the raid/group
	BasketToolsDKPClasses = {["warrior"] = {} , ["druid"] = {} , ["priest"] = {} , ["mage"] = {} , ["warlock"] = {} , ["paladin"] = {} , ["shaman"] = {} , ["hunter"] = {} , ["rogue"] = {}  }
	for key, val in pairs(BasketToolsDKPClasses) do
		if(BasketTools_CheckMessage(what, key)) then
			for i=1 , table.getn(groupMembers) do
				local name = groupMembers[i][1];
				local class = groupMembers[i][2];
				if(string.lower(class) == string.lower(key) and history[name] == nil) then					
					if(BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] == nil) then 
						if not (BasketToolsDKPGetAssociation( name ) == nil) and not ( BasketToolsDKPCharactersFive[BasketToolsDKPGetAssociation( name ) .. BasketToolsDKPGetAccount()] == nil) and not ( BasketToolsDKPGetAssociation( name )  == name) then
							temp = { BasketToolsDKPGetAssociation( name ) , BasketToolsDKPCharactersFive[BasketToolsDKPGetAssociation( name ) .. BasketToolsDKPGetAccount()] }
						else
							BasketToolsDKPRequestAlias( name )
							temp = "?"
						end
					else temp = BasketToolsDKPCharactersFive[name .. BasketToolsDKPGetAccount()] end
					history[name] = true;
					characters[table.getn(dkp)+1] = name 
					-- .. BasketToolsDKPGetAccount()
					dkp[table.getn(dkp)+1] = temp
				end
			end
		end	
	end
	
	-- Check list of characters
	for i=1 , table.getn(msglist) do
		for key, val in pairs(BasketToolsDKPCharactersFive) do
			if(history[key] == nil and BasketTools_CheckMessage(key, BasketToolsDKPGetAccount()) and ( BasketTools_CheckMessage(msglist[i], key) or BasketTools_CheckMessage(key, msglist[i]))) then
				history[key] = true;
				characters[table.getn(dkp)+1] = string.gsub(key, BasketToolsDKPGetAccount(), "")
				dkp[table.getn(dkp)+1] = val
			end
		end
		for key, val in pairs(BasketToolsDKPGetAllAssociation()) do
			if(history[key .. BasketToolsDKPGetAccount()] == nil and ( BasketTools_CheckMessage(msglist[i], key) or BasketTools_CheckMessage(key, msglist[i]))) then
				history[key .. BasketToolsDKPGetAccount()] = true;
				local tmpdkp = 0;
				if not (BasketToolsDKPCharactersFive[val .. BasketToolsDKPGetAccount()] == nil) then tmpdkp = BasketToolsDKPCharactersFive[val .. BasketToolsDKPGetAccount()]
				else tmpdkp = "?" end
				characters[table.getn(dkp)+1] = key
				-- .. BasketToolsDKPGetAccount()
				if(key == val) then
					dkp[table.getn(dkp)+1] = tmpdkp
				else
					dkp[table.getn(dkp)+1] = { val  , tmpdkp }
					-- .. BasketToolsDKPGetAccount()
				end
			end
		end
	end

	return BasketToolsDKP_SortResults( characters , dkp )
end

function BasketToolsDKP_SortResults( characters , dkp )
	-- Bubble Sort (ish)
	local charactersOut = {};
	local messages = {};
	local dkpOut = {};
	local length = table.getn(characters);
	for i=1 , length do
		local highest = 1;
		for k=1 , table.getn(characters) do
			local thisone = 0;
			if not (string.lower(type(dkp[k])) == "table") then thisone = dkp[k];
			else thisone = dkp[k][2]; end    -- if it's from an alias
			
			local highestone = 0;			
			if not (string.lower(type(dkp[highest])) == "table") then highestone = dkp[highest];
			else highestone = dkp[highest][2]; end -- if it's from an alias
			
			if thisone == "?" or thisone == nil then thisone = -10000 end
			if highestone == "?" or highestone == nil then highestone = -10000 end
			if tonumber(thisone) > tonumber(highestone) then highest = k end	
		end
		charactersOut[table.getn(charactersOut)+1] = characters[highest];
		dkpOut[table.getn(dkpOut)+1] = dkp[highest];
		if Debug then DEFAULT_CHAT_FRAME:AddMessage("Sort : " .. characters[highest] .. " (" ..  dkp[highest] .. ")") end
		table.remove(characters,highest);
		table.remove(dkp,highest);		
	end
	
	messages = {};
	for i=1 , table.getn(charactersOut) do
		if not (string.lower(type(dkpOut[i])) == "table") then
			messages[table.getn(messages)+1] = charactersOut[i] .. " (" .. dkpOut[i] .. ")";	
		else
			messages[table.getn(messages)+1] = charactersOut[i] .. "->" .. dkpOut[i][1] .. " (" .. dkpOut[i][2] .. ")";	
		end
	end
	return messages;
end

function BasketToolsDKP_RequestAlias( name )
	BasketToolsDKPRequestAlias( name )
end


--[[
function BasketToolsDKP_SendResults( messages , method )	
	-- Send message
	local message = "DKP :";
	local counter = 0;
	for i=1 , table.getn(messages) do
		counter = counter + 1;
		message = message .. " " .. messages[i];	
		if(counter == 4) then
			if(method == "WHISPER") then
				SendChatMessage(message, method, this.language, who);
			elseif(method == "COMMANDLINE") then
				DEFAULT_CHAT_FRAME:AddMessage(message);
			else
				SendChatMessage(message, method);
			end;
			message = "DKP :";
			counter = 0;
		end
	end
	if(counter > 0) then
		if(method == "WHISPER") then
			SendChatMessage(message, method, this.language, who);
		elseif(method == "COMMANDLINE") then
			DEFAULT_CHAT_FRAME:AddMessage(message);
		else
			SendChatMessage(message, method);
		end
	end
end
]]--

-------------------------
--  Display Functions  --
-------------------------

function BasketToolsDKP_SetSearch( input )
	DKPSearch = input
end

function BasketToolsDKP_DisplayResults( )
	if not ( DKPSearch == "" ) then
		local messages = BasketToolsDKP_Respond( DKPSearch , nil);
		local temp = "Results for \"" .. DKPSearch .. "\" in "
		if BasketToolsDKPGetAccount() == "xmcx" then
			temp = temp .. " MC DKP:\n"
		elseif BasketToolsDKPGetAccount() == "xbwlx" then
			temp = temp .. " BWL DKP:\n"
		else
			temp = temp .. " ERORR:\n"
		end
		for i=1 , table.getn(messages) do
			temp = temp .. messages[i] .. "\n";
		end	
		GuiHeightCurrent = GuiHeightWithResults;
		GuiShowResultsBox = true;
		BasketToolsDKP_Minimize( false );
		--BasketToolsDKPFrame:SetHeight(GuiHeightCurrent);
		BasketToolsDKPFrameResults:SetText(temp);	
		BasketToolsDKPFrameScrollFrame:UpdateScrollChildRect();
		--BasketToolsDKPFrameScrollChild:SetValue(0);
	end
end

function BasketToolsDKP_HideResults( )
	GuiHeightCurrent = GuiHeightWithoutResults;
	GuiShowResultsBox = false;
	DKPSearch = ""
	BasketToolsDKP_Minimize( false );
end
	

function BasketToolsDKPOptionsMenuToggle(  )
	if GuiShowOptionsBox then BasketToolsDKPOptionsMenu( false )
	else BasketToolsDKPOptionsMenu( true ) end
end

function BasketToolsDKPOptionsMenu( state )
	GuiShowOptionsBox = state;
	if state then BasketToolsDKPOptionsFrame:Show() 
	else BasketToolsDKPOptionsFrame:Hide() end
end

function BasketToolsDKP_ToggleDisplay()
	if(BasketToolsDKPFrame:IsVisible()) then BasketToolsDKP_Hide()
	else BasketToolsDKP_Show() end
end

function BasketToolsDKP_Hide()
	if BasketToolsDKPFrame:IsVisible() then
		if Debug then DEFAULT_CHAT_FRAME:AddMessage("BTDKP Hide") end
		BasketToolsDKPSetSettings("gui","hidden",1);
		BasketToolsDKPFrame:Hide();
		BasketToolsDKPFrameNewVersionAvailable:Hide();
		if(CT_RegisterMod) then CT_SetModStatus(modName, "off")	end
	end
end

function BasketToolsDKP_Show()
	if not BasketToolsDKPFrame:IsVisible() then
		if Debug then DEFAULT_CHAT_FRAME:AddMessage("BTDKP Show") end
		BasketToolsDKPSetSettings("gui","hidden",0);
		--PlaySoundFile("Sound\\interface\\levelup2.wav");
		BasketToolsDKPFrame:Show();
		if not (newVersionAvailable == nil) then
			BasketToolsDKPFrameNewVersionAvailable:Show();
		else
			BasketToolsDKPFrameNewVersionAvailable:Hide();
		end
		if(CT_RegisterMod) then CT_SetModStatus(modName, "on") end
	end
end

function BasketToolsDKP_ToggleMinimize()
	if BasketToolsDKPGetSettings("gui","minimize") == 1 then BasketToolsDKP_Minimize(false)
	else BasketToolsDKP_Minimize(true) end;
end

function BasketToolsDKP_Minimize( state )
	if state then
		-- minimize
		--DEFAULT_CHAT_FRAME:AddMessage("BTDKP Minimize");
		BasketToolsDKPSetSettings("gui","minimize",1);
		BasketToolsDKPFrameScrollFrame:Hide();
		BasketToolsDKPFrameScrollFrameHide:Hide();
		BasketToolsDKPFrame:SetHeight("25");
		BasketToolsDKPFrame:SetWidth("125"); 
				
		BasketToolsDKPFrameMinimizeButton:SetPoint("TOPRIGHT",BasketToolsDKPFrame,"TOPRIGHT",-4,-4); 
		
		--BasketToolsDKPFrame:SetPoint("TOPLEFT",UIParent,"LEFT",128,0);
		
		BasketToolsDKPMainText:Hide();
		
		BasketToolsDKPOptionsFrame:Hide();
		BasketToolsDKPFrameHideButton:Hide();	
		BasketToolsDKPOptionsButton:Hide();
		BasketToolsDKPFrameNewVersionAvailable:Hide();
		BasketToolsDKPSearch:Hide();
		BasketToolsDKPSearchText:Hide();
	else
		-- unminimize
		--DEFAULT_CHAT_FRAME:AddMessage("BTDKP Maximize");
		BasketToolsDKPSetSettings("gui","minimize",0);
		local height = GuiHeightCurrent;
		if(GuiShowResultsBox) then
			BasketToolsDKPFrameScrollFrame:Show()
			BasketToolsDKPFrameScrollFrameHide:Show()
		else 
			BasketToolsDKPFrameScrollFrame:Hide()
			BasketToolsDKPFrameScrollFrameHide:Hide()
		end
		if GuiShowOptionsBox then height=GuiHeightCurrent+150 end
		BasketToolsDKPFrame:SetHeight(height);
		BasketToolsDKPFrame:SetWidth("232");		
		BasketToolsDKPFrameMinimizeButton:SetPoint("TOPRIGHT",BasketToolsDKPFrameHideButton,"TOPLEFT",-4,0); 
		
		BasketToolsDKPMainText:Show();
		
		BasketToolsDKPOptionsMenu( GuiShowOptionsBox );
		BasketToolsDKPFrameHideButton:Show();	
		BasketToolsDKPSearch:SetText("");
		BasketToolsDKPSearch:Show();
		BasketToolsDKPSearchText:Show();
		BasketToolsDKPOptionsButton:Show();
		
		if not (newVersionAvailable == nil) then BasketToolsDKPFrameNewVersionAvailable:Show()
		else BasketToolsDKPFrameNewVersionAvailable:Hide() end
	end
end

function BasketToolsDKPYourDKP()
	local char = BasketToolsDKPGetSettings( "alias" , nil);
	local dkp = nil;
	if not (char == nil) then
	 	dkp = BasketToolsDKPCharactersFive[char .. BasketToolsDKPGetAccount()];
	end
	
	if debug then DEFAULT_CHAT_FRAME:AddMessage("Debug : " .. char .. " , " .. BasketToolsDKPGetAccount()) end
	
	
	if dkp == nil then dkp = "?"; end
	BasketToolsDKPYourDKPText:SetText("DKP : " .. dkp);	
end

function GetBMRollBoost()
	return BMRollBoost
end

function BasketToolsDKPRoll()
   -- C/O Belligerence
   local char = BasketToolsDKPGetSettings( "alias" , nil);
   local dkp = BasketToolsDKPCharactersFive[char .. BasketToolsDKPGetAccount()];
   if dkp == nil then dkp = 0 end

   local floor = math.floor(dkp);
   if (floor <= dkp and dkp < floor + 0.5) then
      dkp = floor;
   else
      dkp = floor + 1;
   end

   if not (char == Player) then
      if GetNumRaidMembers() > 0 then
         SendChatMessage("Rolling as " .. char .. " ( " .. dkp.. " dkp)", "RAID");
      elseif GetNumPartyMembers()>0 then
         SendChatMessage("Rolling as " .. char .. " ( " .. dkp.. " dkp)", "PARTY");
      end      
   end
   
  	if( BasketToolsDKPGetAccount() == "xmcx" ) then
		if dkp >= 50 then
			RandomRoll(1,100)
		elseif dkp > 0 then
			RandomRoll(1,dkp)
		else
			RandomRoll(1,1)
		end
	else	
	   if dkp >= 200 then
		  RandomRoll(1, 200);
	   elseif (dkp > 0) then
		  RandomRoll(1, dkp);
	   else
		  RandomRoll(1,1);
	   end
	end
end 
